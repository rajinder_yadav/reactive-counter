import { RxjsCounterPage } from './app.po';

describe('rxjs-counter App', () => {
  let page: RxjsCounterPage;

  beforeEach(() => {
    page = new RxjsCounterPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
