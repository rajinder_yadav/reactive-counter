import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/interval';

import 'rxjs/add/operator/scan';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/takeUntil';
import 'rxjs/add/operator/switchMapTo';
import 'rxjs/add/operator/mapTo';

interface CounterData {
  count: number;
}

interface CounterFunc {
  (arg: CounterData);
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Reactive Counter';
  count = 0;

  // Observable (streams)
  start$: Subject<number>;
  stop$: Subject<number>;
  reset$: Subject<number>;

  counter$: Observable<CounterData>;
  intervalUntil$: Observable<number>;

  constructor() {
    this.start$ = new Subject();
    this.stop$ = new Subject();
    this.reset$ = new Subject();

    // Observable that cancels on a stream.
    this.intervalUntil$ = Observable
      .interval(500)
      .takeUntil(this.stop$);

    // Notice that takeUntil is on the inner Observable, putting it on the outer Observable
   // would kill the entire Observable chain, and we would need to re-subscribe on it again.
    this.counter$ = this.start$
      .switchMapTo(Observable.merge(
          this.intervalUntil$.mapTo(this.incCount),
          this.reset$.mapTo(this.resetCount)
      ))
      .startWith(this.resetCount)
      .scan((acc: CounterData, curr: CounterFunc) => curr(acc), {count: 0});

    // Assign observer to cancelable interval stream.
    this.counter$
      .subscribe((v: CounterData) => {
        this.count = v.count;
        console.log(v);
      });
  }

  resetCount(v: CounterData) {
    return {count: 0};
  }

  incCount(v: CounterData) {
    return {count: v.count + 1};
  }

  onStart() {
    this.start$.next();
  }

  onStop() {
    this.stop$.next();
  }

  onReset() {
    this.reset$.next();
  }

}
