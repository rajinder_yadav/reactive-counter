![Image](./logo.png)

# Reactive Counter

Angular 4 application using RxJS

Showing usage of

1. Observable
1. Subject

Showing usage of the following operators:

* takeUntil
* switchMapTo
* mapTo
* merge
* scan

Also shows how to map an Observable to a function and use with scan to perform different operations in a functional way vs imparative way using subscribe.
